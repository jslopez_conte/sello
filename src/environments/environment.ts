// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://sde-dev.fonticapp.com/api',
  imgUrl: 'https://sde-dev.fonticapp.com',
  subMenus: {
    general: [
      { 
        name: 'Logos',
        route: '/admin/general/logos'
      },
      {
        name: 'Banners',
        route: '/admin/general/banners'
      },
      {
        name: 'Pie de Página',
        route: '/admin/general/footer'
      }
    ],
    applications: [
      { 
        name: 'No Otorgados',
        route: '/admin/applications/no-awarded'
      }
    ],
    manage: [
      { 
        name: 'Tiempo/Requisito',
        route: '/admin/manage/time-req'
      },
      {  
        name: 'Ciudades',
        route: '/admin/manage/cities'
      },
      { 
        name: 'Regiones',
        route: '/admin/manage/regions'
      },
      { 
        name: 'Perfiles',
        route: '/admin/manage/profiles'
      }
    ]
  }
};
