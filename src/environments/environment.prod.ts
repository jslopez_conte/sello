export const environment = {
  production: true,
  apiUrl: 'https://sde-dev.fonticapp.com/api',
  imgUrl: 'https://sde-dev.fonticapp.com',
  subMenus: {
    general: [
      { 
        name: 'Logos',
        route: '/admin/general/logos'
      },
      {
        name: 'Banners',
        route: '/admin/general/banners'
      },
      {
        name: 'Pie de Página',
        route: '/admin/general/footer'
      }
    ],
    applications: [
      { 
        name: 'No Otorgados',
        route: '/admin/applications/no-awarded'
      }
    ],
    manage: [
      { 
        name: 'Tiempo/Requisito',
        route: '/admin/manage/time-req'
      }
    ]
  }
};
