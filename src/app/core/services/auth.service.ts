import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/app/config/config';
import { RequestLogin } from '../models/request-login';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = Config.env.apiUrl;

  constructor(
    private http: HttpClient,
    private router: Router) { }


  login(requestLogin: RequestLogin) {
    return this.http.post<any>(`${this.baseUrl}/auth/login`, requestLogin);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUserId');
    localStorage.removeItem('last_access');

    this.router.navigateByUrl('/');
  }

  isUserLogged(){
    if( localStorage.getItem('token') ) return true;
    else return false;
  }

}
