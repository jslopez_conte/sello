import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestLogin } from '../../models/request-login';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  loading: boolean;
  errorMessage: string;
  returnUrl: string;
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) 
  {
    this.loading = false;
    this.errorMessage = '';
    this.returnUrl = '';
    this.form = new FormGroup({});
   }


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['url'] || '/admin';

    if( this.authService.isUserLogged() ) this.router.navigateByUrl(this.returnUrl);

  }

  login() {
    this.loading = true;
    this.errorMessage = '';

    const username = this.form.controls['email'].value;
    const password = this.form.controls['password'].value;

    const requestLogin: RequestLogin = {
      email: username,
      password: password
    }

    this.authService.login(requestLogin).subscribe((response: any) => {
      
      if (response.error.htmlCode == 200) {
        localStorage.setItem('token', response.token);
        localStorage.setItem('currentUserId', response.user_id);
        localStorage.setItem('last_access', JSON.stringify(new Date()));

        this.router.navigateByUrl(this.returnUrl);

      }
      this.loading = false;

    }, ({error:{ error }}: any) => {
      if( error && error.message === 'NO ERROR'){
        this.errorMessage = 'Se ha presentado un error, por favor contacte al administrador';
      }
      else {
        this.errorMessage = 'Usuario y/o contraseña incorrectos';
        
      }
      
      this.loading = false;
    })
    
  }

}
