export interface ResponseRequestProfiles {
   data: Profile[]
   total_results: number
 }
 
 export interface Profile {
   id: number
   name: string
 }
 