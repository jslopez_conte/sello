
export interface ResponseRequestCountries {
   data: Country[]
   total_results: number
 }
 
 export interface Country {
   id: number
   name: string
   nacionalidad?: string
   id_capital?: number
   capital: Capital
 }
 
 export interface Capital {
   id?: number
   name?: string
   code: any
   latitude: any
   longitude: any
   id_region?: number
 }
 