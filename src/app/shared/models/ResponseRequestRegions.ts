export interface ResponseRequestRegions {
   data: Region[]
   total_results: number
 }
 
 export interface Region {
   id: number
   name: string
   id_capital?: number
   id_country: number
   code?: string
   country: Country
   capital: Capital
 }
 
 export interface Country {
   id: number
   name: string
   nacionalidad?: string
   id_capital?: number
 }
 
 export interface Capital {
   id?: number
   name?: string
   code: any
   latitude: any
   longitude: any
   id_region?: number
 }
 