export interface ResponseRequestCities {
  data: City[]
  total_results: number
}

export interface City {
  id: number
  name: string
  code: any
  latitude: any
  longitude: any
  id_region: number
  region: Region
}

export interface Region {
  id: number
  name: string
  id_capital: any
  id_country: number
  code: any
}