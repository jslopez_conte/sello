import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  
  @ViewChild('fileUpload') fileUpload: any;
  @Output() selectedFilesEvent = new EventEmitter<File[]>();
  @Output() filesRemoved = new EventEmitter<boolean>();
  @Input() multiple: boolean = false;
  @Input() activateDeleteBtn: boolean = false;
  @Input() ifFileLoadedDisabled: boolean = false; //Si carga un archivo se bloquea el boton para volver a cargar otro
  @Input() textValue: string = "";
  
  
  public selectedFiles: File[] = []; 
  public disabled: boolean = false;
  public loading: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  addFile() {
    this.fileUpload.nativeElement.click();
  }

  removeFile() {
    this.selectedFiles = [];   
    this.selectedFilesEvent.emit([]);
    this.filesRemoved.emit(true);
  }

  handleFileInput( event: any ) {
    if( !event || !event.target || !event.target.files ) return
    let files: Array<File> = event.target.files;

    this.selectedFiles = Array.from(files);
    this.selectedFilesEvent.emit(this.selectedFiles);
    
    if( this.ifFileLoadedDisabled ){
      if(this.selectedFiles.length > 0){
        this.disabled = true;
      }
      else {
        this.disabled = false;
      }
    }
   
  }

}
