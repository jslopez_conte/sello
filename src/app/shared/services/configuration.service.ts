import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Config } from 'src/app/config/config';
import { ResponseRequestProfiles } from '../models/ResponseRequestProfiles';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private baseUrl: string = Config.env.apiUrl;
  
  constructor(
    private http: HttpClient
  ) { }

  private getQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.get(url, { headers });
  }

  private putQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.put(url, body,{ headers });
  }

  private postQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.post(url, body,{ headers });
  }

  getProfiles(page: string = "1"): Observable<ResponseRequestProfiles> {
    return this.getQuery('/configuration/role?simple=false&limit=10&page='+page)
      .pipe( map((data: any) => data));
  }
  
}
