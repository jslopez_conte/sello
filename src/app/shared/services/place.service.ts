import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Config } from 'src/app/config/config';
import { City, ResponseRequestCities } from '../models/ResponseRequestCities';
import { Country, ResponseRequestCountries } from '../models/ResponseRequestCountries';
import { Region, ResponseRequestRegions } from '../models/ResponseRequestRegions';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  private baseUrl: string = Config.env.apiUrl;
  
  constructor(
    private http: HttpClient
  ) { }

  private getQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.get(url, { headers });
  }

  private putQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.put(url, body,{ headers });
  }

  private postQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.post(url, body,{ headers });
  }

  getCities(page: string = "1"): Observable<ResponseRequestCities> {
    return this.getQuery('/place/city?filter=&order=id&limit=20&page='+page+'&lang=1')
      .pipe( map((data: any) => data));
  }

  getCityById(cityId: string): Observable<ResponseRequestCities> {
    return this.getQuery('/place/city?simple=false&filter_field=id&filter_value='+cityId+'&limit=10&page=1')
      .pipe( map((data: any) => data));
  }

  updateCity(body: any): Observable<City> {
    return this.putQuery('/place/city', body)
      .pipe( map((data: any) => data));
  }

  saveCity(body: any): Observable<City> {
    return this.postQuery('/place/city', body)
      .pipe( map((data: any) => data));
  }

  getRegions(page: string = "1"): Observable<ResponseRequestRegions> {
    return this.getQuery('/place/region?filter=&order=id&limit=20&page='+page+'&lang=1')
      .pipe( map((data: any) => data));
  }

  getRegionById(regionId: string): Observable<ResponseRequestRegions> {
    return this.getQuery('/place/region?simple=false&filter_field=id&filter_value='+regionId+'&limit=10&page=1')
      .pipe( map((data: any) => data));
  }

  getCountries(page: string = "1"): Observable<ResponseRequestCountries> {
    return this.getQuery('/place/country?simple=false&limit=5000&page='+page)
      .pipe( map((data: any) => data));
  }


  updateRegion(body: any): Observable<Region> {
    return this.putQuery('/place/region', body)
      .pipe( map((data: any) => data));
  }

  saveRegion(body: any): Observable<Country> {
    return this.postQuery('/place/region', body)
      .pipe( map((data: any) => data));
  }

  
}
