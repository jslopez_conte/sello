
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from './core/core.module';
import { AuthGuard } from './core/guards/auth.guard';
import { HomeComponent } from './core/pages/home/home.component';
import { PrivateLayoutComponent } from './layout/private-layout/private-layout.component';
import { PublicLayoutComponent } from './layout/public-layout/public-layout.component';

const APP_ROUTING: Routes = [
    {
        path: 'auth',
        component: PublicLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: () => CoreModule
            },
            {
                path: '**',
                redirectTo: 'login'
            }
        ]
    },
    {
        path: 'admin',
        canActivate: [AuthGuard],
        component: PrivateLayoutComponent,
        children: [
            {
                path: 'home',
                component: HomeComponent
            },
            {
                path: 'general',
                loadChildren: () => import('./modules/general/general.module').then(m => m.GeneralModule)
            },
            {
                path: 'applications',
                loadChildren: () => import('./modules/applications/applications.module').then(m => m.ApplicationsModule)
            },
            {
                path: 'manage',
                loadChildren: () => import('./modules/manage/manage.module').then(m => m.ManageModule)
            },
            {
                path: '**',
                redirectTo: 'home'
            }
        ]
        
    },
    {
        path: '**',
        redirectTo: 'auth'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTING, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
