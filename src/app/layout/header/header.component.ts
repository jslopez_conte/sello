import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Config } from 'src/app/config/config';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  public isGeneralMenu: boolean = false;
  public isApplicationsMenu: boolean = false;
  public isManageMenu: boolean = false;
  public listItemSubmenu: any = Config.env.subMenus;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    console.log('ACTIVE:', this.router.url);

    let routeCode = '';
    if( this.router.url.includes('general') ) routeCode = 'GEN';
    else if( this.router.url.includes('applications') ) routeCode = 'APP';
    else if( this.router.url.includes('manage') ) routeCode = 'MAN';
    this.changeSubMenu(routeCode);
    
  }


  changeSubMenu(selectedMenu: any) {
    
    this.hideSubmenus();

    switch (selectedMenu) {
      case 'GEN':
        this.isGeneralMenu = true;
        break;
      case 'APP':
        this.isApplicationsMenu = true;
        break;
      case 'MAN':
        this.isManageMenu = true;
        break;
      default:
        
        break;
    }    
  }

  hideSubmenus(){
    this.isGeneralMenu = false;
    this.isApplicationsMenu = false;
    this.isManageMenu = false;
  }

  logout() {
    this.authService.logout();
  }

}
