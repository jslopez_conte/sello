import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { RouterModule } from '@angular/router';
import { PrivateLayoutComponent } from './private-layout/private-layout.component';
import { HeaderComponent } from './header/header.component';
import { SubMenuComponent } from './sub-menu/sub-menu.component';



@NgModule({
  declarations: [
    PublicLayoutComponent,
    PrivateLayoutComponent,
    HeaderComponent,
    SubMenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([])
  ],
  exports: [ PublicLayoutComponent]
})
export class LayoutModule { }
