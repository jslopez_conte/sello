import { environment } from "src/environments/environment";


export class Config {
    static title = 'SELLO';
    static env = environment;
    


    get environment() {
        return Config.env;
    }
  
}