

export interface RequestStatus {
   id: number;
   name: string;
   duration: number;
   pre_end: number;
   alert: number;
   description: string;
}