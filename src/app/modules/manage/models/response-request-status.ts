import { RequestStatus } from "./request-status";

export interface ResponseRequestStatus {
   data: Array<RequestStatus>;
   total_results: number;
}
