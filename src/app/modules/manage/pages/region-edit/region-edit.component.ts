import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { City, ResponseRequestCities } from 'src/app/shared/models/ResponseRequestCities';
import { Country, ResponseRequestCountries } from 'src/app/shared/models/ResponseRequestCountries';
import { Region, ResponseRequestRegions } from 'src/app/shared/models/ResponseRequestRegions';
import { PlaceService } from 'src/app/shared/services/place.service';

@Component({
  selector: 'app-region-edit',
  templateUrl: './region-edit.component.html',
  styleUrls: ['./region-edit.component.scss']
})
export class RegionEditComponent implements OnInit {

  public requestStatusSelected: Array<Region> = [];
  public requestStatusForm: FormGroup = new FormGroup({});
  public regions: Array<Region> = [];
  public countries: Array<Country> = [];
  public capitals: Array<City> = [];
  private cityFormData: FormData = new FormData();


  constructor(
    private activeRouter: ActivatedRoute,
    private placeService: PlaceService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe((params: any) => {
      this.loadRequesStatusSelected(params.id);
    });

    this.loadRegions();

    this.loadCountries();

    this.loadCapitals();


    this.requestStatusForm = this.formBuilder.group({
      id: [0, Validators.min(0)],
      name: ['', Validators.required],
      country: [0, Validators.min(0)],
      capital: [0, Validators.min(0)]
    });

  }

  loadRequesStatusSelected(requestStatusId: string) {
    this.placeService.getRegionById( requestStatusId ).subscribe((response: ResponseRequestRegions) => {

      response.data.forEach( (requestStatus: Region) => {
        if( requestStatus.id === parseInt(requestStatusId)) this.requestStatusSelected.push(requestStatus);
      });

      if( !this.requestStatusSelected.length ) return;

      console.log('Response: ', this.requestStatusSelected[0]);

      this.requestStatusForm.patchValue({
        id: this.requestStatusSelected[0].id,
        name: this.requestStatusSelected[0].name,
        country: this.requestStatusSelected[0].country.id,
        capital: this.requestStatusSelected[0].capital.id
      });

      this.cityFormData.append('id', this.requestStatusSelected[0].id.toString());
      this.cityFormData.append('name', this.requestStatusSelected[0].name);
      this.cityFormData.append('id_country', this.requestStatusSelected[0].country.id.toString());
      this.cityFormData.append('id_capital', this.requestStatusSelected[0].capital.id ? this.requestStatusSelected[0].capital.id.toString() : '');

    });
  }

  loadRegions(){
    this.placeService.getRegions().subscribe((response: ResponseRequestRegions) => {
      this.regions = [...response.data];
    });
  }

  loadCountries(){
    this.placeService.getCountries().subscribe((response: ResponseRequestCountries) => {
      this.countries = [...response.data];
    });
  }

  loadCapitals(){
    this.placeService.getCities().subscribe((response: ResponseRequestCities) => {
      this.capitals = [...response.data];
    });
  }

  updateCity() {
    this.cityFormData.set('id', this.requestStatusForm.controls['id'].value);
    this.cityFormData.set('name', this.requestStatusForm.controls['name'].value);
    this.cityFormData.set('id_country', this.requestStatusForm.controls['country'].value);
    this.cityFormData.set('id_capital', this.requestStatusForm.controls['capital'].value);
   
    this.placeService.updateRegion( this.cityFormData ).subscribe( (response: Region) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/manage/regions'])
        
      });
      
    })

  }

   /**Comparaciones para los selects*/
   compareObjects(o1: any, o2: any) {
    console.log(o1);
    console.log(o2);
    
    
    if (o2 !== null) {
      return o1 === o2;
    }
    return false;
  }

}
