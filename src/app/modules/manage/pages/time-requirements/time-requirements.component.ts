import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Banner } from 'src/app/modules/general/models/banner';
import { ResponseBanner } from 'src/app/modules/general/models/response-banner';
import { PlatformService } from 'src/app/modules/general/services/platform.service';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import { RequestStatus } from '../../models/request-status';
import { ResponseRequestStatus } from '../../models/response-request-status';
import { QuestionService } from '../../services/question.service';

@Component({
  selector: 'app-time-requirements',
  templateUrl: './time-requirements.component.html',
  styleUrls: ['./time-requirements.component.scss']
})
export class TimeRequirementsComponent implements OnInit {

   public requestStatusList: Array<RequestStatus> = [];
	public tmpRequestStatusList: Array<RequestStatus> = [];
	public canShowMore: boolean = false;
	public numberPage: number = 1;
	public types: Array<any> = [];
	public searchForm: FormGroup = new FormGroup({});;

	constructor(
		private questionService: QuestionService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog
	) { }
 
	ngOnInit(): void {
		this.searchForm = this.formBuilder.group({
			type: [''],
			active: [null],
			search: ['']
		});

		this.loadRequestStatus();
	}

	loadRequestStatus() {
		this.questionService.getRequestStatus(this.numberPage.toString()).subscribe((response: ResponseRequestStatus) => {

			if (response.total_results === 0) return;
			this.requestStatusList = [...this.requestStatusList, ...response.data];
			this.tmpRequestStatusList = [...this.requestStatusList];

			if (response.total_results > this.requestStatusList.length) {
				this.canShowMore = true;
				this.numberPage++;
			}
			else {
				this.canShowMore = false;
			}
		})
	}


	search() {
		/*
		let search = this.searchForm.controls['search'].value.trim().toLowerCase();

		if (search !== '') {
			this.requestStatusList = this.requestStatusList.filter((requestStatus: RequestStatus) => {

				let title = requestStatus.title;
				let summary = requestStatus.summary;

				if (!title) title = '';
				if (!summary) summary = '';


				if (title.toLowerCase().includes(search) || summary.toLowerCase().includes(search)) return banner;
				else return;
			})
		}
		else {
			this.requestStatusList = [...this.tmpRequestStatusList];
		}*/

	}
	
	

}
