import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeRequirementsComponent } from './time-requirements.component';

describe('TimeRequirementsComponent', () => {
  let component: TimeRequirementsComponent;
  let fixture: ComponentFixture<TimeRequirementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeRequirementsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimeRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
