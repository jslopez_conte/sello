import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Config } from 'src/app/config/config';
import { Banner } from 'src/app/modules/general/models/banner';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { RequestStatus } from '../../models/request-status';
import { ResponseRequestStatus } from '../../models/response-request-status';
import { QuestionService } from '../../services/question.service';

@Component({
  selector: 'app-edit-time-requirements',
  templateUrl: './edit-time-requirements.component.html',
  styleUrls: ['./edit-time-requirements.component.scss']
})
export class EditTimeRequirementsComponent implements OnInit {

  public requestStatusSelected: Array<RequestStatus> = [];
  public requestStatusForm: FormGroup = new FormGroup({});


  constructor(
    private activeRouter: ActivatedRoute,
    private questionService: QuestionService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe((params: any) => {
      this.loadRequesStatusSelected(params.id);
    });


    this.requestStatusForm = this.formBuilder.group({
      duration: [0, Validators.min(0)],
      pre_end: [0, Validators.min(0)],
      alert: [false, Validators.required],
      description: ['', Validators.required]
    });

  }

  loadRequesStatusSelected(requestStatusId: string) {
    this.questionService.getRequestStatus().subscribe((response: ResponseRequestStatus) => {

      response.data.forEach( (requestStatus: RequestStatus) => {
        if( requestStatus.id === parseInt(requestStatusId)) this.requestStatusSelected.push(requestStatus);
      });

      if( !this.requestStatusSelected.length ) return;

      console.log('Response: ', this.requestStatusSelected[0]);

      this.requestStatusForm.patchValue({
        duration: this.requestStatusSelected[0].duration,
        pre_end: this.requestStatusSelected[0].pre_end,
        alert: this.requestStatusSelected[0].alert,
        description: this.requestStatusSelected[0].description
      });

    });
  }

  updateBanner() {
    this.requestStatusSelected[0].duration =  this.requestStatusForm.controls['duration'].value;
    this.requestStatusSelected[0].pre_end = this.requestStatusForm.controls['pre_end'].value;
    this.requestStatusSelected[0].alert = this.requestStatusForm.controls['alert'].value;
    this.requestStatusSelected[0].description = this.requestStatusForm.controls['description'].value;

    this.questionService.updateRequestStatus(this.requestStatusSelected[0]).subscribe( (response: RequestStatus) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/manage/time-req'])
        
      });
      
    })

  }



}
