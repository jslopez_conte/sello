import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTimeRequirementsComponent } from './edit-time-requirements.component';

describe('EditTimeRequirementsComponent', () => {
  let component: EditTimeRequirementsComponent;
  let fixture: ComponentFixture<EditTimeRequirementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTimeRequirementsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditTimeRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
