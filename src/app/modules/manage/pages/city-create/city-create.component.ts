import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { City, ResponseRequestCities } from 'src/app/shared/models/ResponseRequestCities';
import { Region, ResponseRequestRegions } from 'src/app/shared/models/ResponseRequestRegions';
import { PlaceService } from 'src/app/shared/services/place.service';

@Component({
  selector: 'app-city-create',
  templateUrl: './city-create.component.html',
  styleUrls: ['./city-create.component.scss']
})
export class CityCreateComponent implements OnInit {

  public requestStatusSelected: Array<City> = [];
  public requestStatusForm: FormGroup = new FormGroup({});
  public regions: Array<Region> = [];
  private cityFormData: FormData = new FormData();


  constructor(
    private activeRouter: ActivatedRoute,
    private placeService: PlaceService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadRegions();


    this.requestStatusForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      latitude: [0, Validators.min(0)],
      longitude: [0, Validators.min(0)],
      region: [0, Validators.min(0)]
    });

  }

  loadRegions(){
    this.placeService.getRegions().subscribe((response: ResponseRequestRegions) => {
      this.regions = [...response.data];
    });
  }

  updateCity() {
    this.cityFormData.set('code', this.requestStatusForm.controls['code'].value);
    this.cityFormData.set('name', this.requestStatusForm.controls['name'].value);
    this.cityFormData.set('id_region', this.requestStatusForm.controls['region'].value);
    this.cityFormData.set('latitude', this.requestStatusForm.controls['latitude'].value);
    this.cityFormData.set('longitude', this.requestStatusForm.controls['longitude'].value);
   
    this.placeService.saveCity(this.cityFormData).subscribe( (response: City) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/manage/cities'])
        
      });
      
    })

  }

   /**Comparaciones para los selects*/
   compareObjects(o1: any, o2: any) {
    console.log(o1);
    console.log(o2);
    
    
    if (o2 !== null) {
      return o1 === o2;
    }
    return false;
  }

}
