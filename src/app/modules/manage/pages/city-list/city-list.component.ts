import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { City, ResponseRequestCities } from 'src/app/shared/models/ResponseRequestCities';
import { PlaceService } from 'src/app/shared/services/place.service';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent implements OnInit {

  public requestStatusList: Array<City> = [];
	public tmpRequestStatusList: Array<City> = [];
	public canShowMore: boolean = false;
	public numberPage: number = 1;
	public types: Array<any> = [];
	public searchForm: FormGroup = new FormGroup({});;

	constructor(
		private placeService: PlaceService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog
	) { }
 
	ngOnInit(): void {
		this.searchForm = this.formBuilder.group({
			type: [''],
			active: [null],
			search: ['']
		});

		this.loadRequestStatus();
	}

	loadRequestStatus() {
		this.placeService.getCities(this.numberPage.toString()).subscribe((response: ResponseRequestCities) => {

			if (response.total_results === 0) return;
			this.requestStatusList = [...this.requestStatusList, ...response.data];
			this.tmpRequestStatusList = [...this.requestStatusList];

			if (response.total_results > this.requestStatusList.length) {
				this.canShowMore = true;
				this.numberPage++;
			}
			else {
				this.canShowMore = false;
			}
		})
	}

}
