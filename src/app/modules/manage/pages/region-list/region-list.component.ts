import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { City, ResponseRequestCities } from 'src/app/shared/models/ResponseRequestCities';
import { Region, ResponseRequestRegions } from 'src/app/shared/models/ResponseRequestRegions';
import { PlaceService } from 'src/app/shared/services/place.service';

@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent implements OnInit {

  public requestStatusList: Array<Region> = [];
	public tmpRequestStatusList: Array<Region> = [];
	public canShowMore: boolean = false;
	public numberPage: number = 1;
	public types: Array<any> = [];
	public searchForm: FormGroup = new FormGroup({});;

	constructor(
		private placeService: PlaceService,
		private formBuilder: FormBuilder
	) { }
 
	ngOnInit(): void {
		this.searchForm = this.formBuilder.group({
			type: [''],
			active: [null],
			search: ['']
		});

		this.loadRequestStatus();
	}

	loadRequestStatus() {
		this.placeService.getRegions(this.numberPage.toString()).subscribe((response: ResponseRequestRegions) => {

			if (response.total_results === 0) return;
			this.requestStatusList = [...this.requestStatusList, ...response.data];
			this.tmpRequestStatusList = [...this.requestStatusList];

			if (response.total_results > this.requestStatusList.length) {
				this.canShowMore = true;
				this.numberPage++;
			}
			else {
				this.canShowMore = false;
			}
		})
	}

}
