import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { City, ResponseRequestCities } from 'src/app/shared/models/ResponseRequestCities';
import { Region, ResponseRequestRegions } from 'src/app/shared/models/ResponseRequestRegions';
import { PlaceService } from 'src/app/shared/services/place.service';

@Component({
  selector: 'app-city-edit',
  templateUrl: './city-edit.component.html',
  styleUrls: ['./city-edit.component.scss']
})
export class CityEditComponent implements OnInit {

  public requestStatusSelected: Array<City> = [];
  public requestStatusForm: FormGroup = new FormGroup({});
  public regions: Array<Region> = [];
  private cityFormData: FormData = new FormData();


  constructor(
    private activeRouter: ActivatedRoute,
    private placeService: PlaceService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe((params: any) => {
      this.loadRequesStatusSelected(params.id);
    });

    this.loadRegions();


    this.requestStatusForm = this.formBuilder.group({
      id: [0, Validators.min(0)],
      name: [0, Validators.min(0)],
      code: [false, Validators.required],
      latitude: [0, Validators.min(0)],
      longitude: [0, Validators.min(0)],
      region: [0, Validators.min(0)]
    });

  }

  loadRequesStatusSelected(requestStatusId: string) {
    this.placeService.getCityById( requestStatusId ).subscribe((response: ResponseRequestCities) => {

      response.data.forEach( (requestStatus: City) => {
        if( requestStatus.id === parseInt(requestStatusId)) this.requestStatusSelected.push(requestStatus);
      });

      if( !this.requestStatusSelected.length ) return;

      console.log('Response: ', this.requestStatusSelected[0]);

      this.requestStatusForm.patchValue({
        id: this.requestStatusSelected[0].id,
        name: this.requestStatusSelected[0].name,
        code: this.requestStatusSelected[0].code,
        latitude: this.requestStatusSelected[0].latitude,
        longitude: this.requestStatusSelected[0].longitude,
        region: this.requestStatusSelected[0].region.id
      });

      this.cityFormData.append('id', this.requestStatusSelected[0].id.toString());
      this.cityFormData.append('name', this.requestStatusSelected[0].name);
      this.cityFormData.append('id_region', this.requestStatusSelected[0].id_region.toString());

    });
  }

  loadRegions(){
    this.placeService.getRegions().subscribe((response: ResponseRequestRegions) => {
      this.regions = [...response.data];
    });
  }

  updateCity() {
    this.cityFormData.set('id', this.requestStatusForm.controls['id'].value);
    this.cityFormData.set('name', this.requestStatusForm.controls['name'].value);
    this.cityFormData.set('id_region', this.requestStatusForm.controls['region'].value);
   
    this.placeService.updateCity(this.cityFormData).subscribe( (response: City) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/manage/cities'])
        
      });
      
    })

  }

   /**Comparaciones para los selects*/
   compareObjects(o1: any, o2: any) {
    console.log(o1);
    console.log(o2);
    
    
    if (o2 !== null) {
      return o1 === o2;
    }
    return false;
  }

}
