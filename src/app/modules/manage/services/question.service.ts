import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Config } from 'src/app/config/config';
import { RequestStatus } from '../models/request-status';
import { ResponseRequestStatus } from '../models/response-request-status';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private baseUrl: string = Config.env.apiUrl;
  
  constructor(
    private http: HttpClient
  ) { }

  private getQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.get(url, { headers });
  }

  private putQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.put(url, body,{ headers });
  }

  private postQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.post(url, body,{ headers });
  }

  private deleteQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.delete(url,{ headers });
  }

  getRequestStatus(page: string = "1"): Observable<ResponseRequestStatus> {
    return this.getQuery('/question/request_status?filter=&order=id&limit=20&page='+page+'&lang=1')
      .pipe( map((data: any) => data));
  }

  updateRequestStatus(body: any): Observable<RequestStatus> {
    return this.putQuery('/question/request_status', body)
      .pipe( map((data: any) => data[0]));
  }


}
