import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityCreateComponent } from './pages/city-create/city-create.component';
import { CityEditComponent } from './pages/city-edit/city-edit.component';
import { CityListComponent } from './pages/city-list/city-list.component';
import { EditTimeRequirementsComponent } from './pages/edit-time-requirements/edit-time-requirements.component';
import { RegionCreateComponent } from './pages/region-create/region-create.component';
import { RegionEditComponent } from './pages/region-edit/region-edit.component';
import { RegionListComponent } from './pages/region-list/region-list.component';
import { TimeRequirementsComponent } from './pages/time-requirements/time-requirements.component';

const MANAGE_ROUTING: Routes = [{
  path: '',
  children: [
    {
      path: 'time-req',
      component: TimeRequirementsComponent,
    },
    {
      path: 'time-req/edit/:id',
      component: EditTimeRequirementsComponent,
    },
    {
      path: 'cities',
      component: CityListComponent,
    },
    {
      path: 'cities/edit/:id',
      component: CityEditComponent,
    },
    {
      path: 'cities/create',
      component: CityCreateComponent,
    },
    {
      path: 'regions',
      component: RegionListComponent,
    },
    {
      path: 'regions/edit/:id',
      component: RegionEditComponent,
    },
    {
      path: 'regions/create',
      component: RegionCreateComponent,
    },
    {
      path: '**',
      redirectTo: 'home'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(MANAGE_ROUTING)],
  exports: [RouterModule]
})
export class ManageRoutingModule { }
