import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';
import { TimeRequirementsComponent } from './pages/time-requirements/time-requirements.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditTimeRequirementsComponent } from './pages/edit-time-requirements/edit-time-requirements.component';
import { CityListComponent } from './pages/city-list/city-list.component';
import { CityEditComponent } from './pages/city-edit/city-edit.component';
import { CityCreateComponent } from './pages/city-create/city-create.component';
import { RegionListComponent } from './pages/region-list/region-list.component';
import { RegionEditComponent } from './pages/region-edit/region-edit.component';
import { RegionCreateComponent } from './pages/region-create/region-create.component';
import { ProfileListComponent } from './pages/profile-list/profile-list.component';


@NgModule({
  declarations: [
    TimeRequirementsComponent,
    EditTimeRequirementsComponent,
    CityListComponent,
    CityEditComponent,
    CityCreateComponent,
    RegionListComponent,
    RegionEditComponent,
    RegionCreateComponent,
    ProfileListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ManageRoutingModule
  ]
})
export class ManageModule { }
