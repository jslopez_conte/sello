import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from 'src/app/config/config';
import { Banner } from '../models/banner';
import { Footer } from '../models/footer';
import { Logo } from '../models/logo';
import { ResponseBanner } from '../models/response-banner';
import { ResponseBannerTypes } from '../models/response-banner-types';
import { ResponseFooter } from '../models/response-footer';

@Injectable({
  providedIn: 'root'
})
export class PlatformService {

  private baseUrl: string = Config.env.apiUrl;


  constructor(
    private http: HttpClient
  ) { }

  getQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.get(url, { headers });
  }

  putQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.put(url, body,{ headers });
  }

  postQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.post(url, body,{ headers });
  }

  deleteQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.delete(url,{ headers });
  }

  getLogos(): Observable<Array<Logo>> {
    return this.getQuery('/platform/config?simple=false&filter_field=id&filter_value=1&limit=10&page=1')
      .pipe( map((data: any) => data['data']));
  }

  updateLogos(body: any): Observable<Array<Logo>> {
    return this.putQuery('/platform/config', body)
      .pipe( map((data: any) => data));
  }

  getBanners(page: string = "1"): Observable<ResponseBanner> {
    return this.getQuery('/platform/banner?simple=false&limit=10&page='+page)
      .pipe( map((data: any) => data));
  }

  getBannerById(bannerId: string): Observable<ResponseBanner> {
    return this.getQuery('/platform/banner?simple=false&filter_field=id&filter_value='+ bannerId+'&limit=10&page=1')
      .pipe( map((data: any) => data));
  }

  getBannerTypes(): Observable<ResponseBannerTypes> {
    return this.getQuery('/platform/type_banner?simple=false&limit=5000&page=1')
      .pipe( map((data: any) => data));
  }

  updateBanner(body: any): Observable<Banner> {
    return this.putQuery('/platform/banner', body)
      .pipe( map((data: any) => data));
  }

  saveBanner(body: any): Observable<Banner> {
    return this.postQuery('/platform/banner', body)
      .pipe( map((data: any) => data));
  }

  deleteBanner(bannerId: any): Observable<any> {
    return this.deleteQuery('/platform/banner?id=' + bannerId)
      .pipe( map((data: any) => data));
  }

  getFooter(): Observable<ResponseFooter> {
    return this.getQuery('/platform/footer?simple=false&filter_field=id&filter_value=1&limit=10&page=1')
      .pipe( map((data: any) => data));
  }

  updateFooter(body: any): Observable<Footer> {
    return this.putQuery('/platform/footer', body)
      .pipe( map((data: any) => data));
  }


}
