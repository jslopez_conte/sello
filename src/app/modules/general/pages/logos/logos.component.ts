import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Config } from 'src/app/config/config';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { Logo } from '../../models/logo';
import { PlatformService } from '../../services/platform.service';

@Component({
  selector: 'app-logos',
  templateUrl: './logos.component.html',
  styleUrls: ['./logos.component.scss']
})
export class LogosComponent implements OnInit {

  public imgUrl = Config.env.imgUrl;
  public logos: Array<Logo> = [];
  public headerLogoSrc: string | SafeResourceUrl = '';
  public footerLogoSrc: string | SafeResourceUrl = '';
  private logosFormData: FormData = new FormData();

  constructor(
    private platformService: PlatformService,
    private sanitizer: DomSanitizer,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loadLogos();

  }

  async loadLogos() {
    this.platformService.getLogos().subscribe((response: Array<Logo>) => {
      this.logosFormData.append('id', response[0].id.toString());
      this.logosFormData.append('header', response[0].header);
      this.logosFormData.append('footer', response[0].footer);

      this.headerLogoSrc = this.imgUrl + response[0].header;
      this.footerLogoSrc = this.imgUrl + response[0].footer;

      this.logos = [...response];
    });
  }

  getLogoHeader(selectedFile: Array<any>) {
    selectedFile.forEach((logoHeader: File) => {
      this.logosFormData.set('header', logoHeader);     

      this.getBase64File(logoHeader).then((base64: any): any => {
        const headerBlob =  this.convertBase64ToBlob(base64.replace(`data:${logoHeader.type};base64,`, ''), logoHeader.type);     
        this.logos[0].header = headerBlob;
        this.headerLogoSrc  = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(headerBlob));
      });

    });
  }
    

  getLogoFooter(selectedFile: Array<any>) {
    selectedFile.forEach((logoFooter: File) => {
      this.logosFormData.set('footer', logoFooter);
      this.getBase64File(logoFooter).then((base64: any): any => {
        const headerBlob =  this.convertBase64ToBlob(base64.replace(`data:${logoFooter.type};base64,`, ''), logoFooter.type);     
        this.logos[0].header = headerBlob;
        this.footerLogoSrc  = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(headerBlob));
      });
    });
  }



  updateLogos() {
    this.platformService.updateLogos(this.logosFormData).subscribe((response: Array<Logo>) => {

      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {
      
        console.log('CLOSE: ', userUpdate);
        
      });


    })
  }

  private getBase64File(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  private convertBase64ToBlob(b64Data: any, contentType = '', sliceSize = 512): Blob {    
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    console.log('Blob: ', blob);
    
    return blob;
  }


}
