import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { Banner } from '../../models/banner';
import { ResponseBanner } from '../../models/response-banner';
import { PlatformService } from '../../services/platform.service';

@Component({
	selector: 'app-banners',
	templateUrl: './banners.component.html',
	styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {

	public bannersList: Array<Banner> = [];
	public tmpBannersList: Array<Banner> = [];
	public canShowMore: boolean = false;
	public numberPage: number = 1;
	public types: Array<any> = [];
	public searchForm: FormGroup = new FormGroup({});;

	constructor(
		private platformService: PlatformService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog
	) { }

	ngOnInit(): void {
		this.searchForm = this.formBuilder.group({
			type: [''],
			active: [null],
			search: ['']
		});

		this.loadBanners();
	}

	loadBanners() {
		this.platformService.getBanners(this.numberPage.toString()).subscribe((response: ResponseBanner) => {

			if (response.total_results === 0) return;
			this.bannersList = [...this.bannersList, ...response.data];
			this.tmpBannersList = [...this.bannersList];
			this.getTypes();

			if (response.total_results > this.bannersList.length) {
				this.canShowMore = true;
				this.numberPage++;
			}
			else {
				this.canShowMore = false;
			}
		})
	}

	getTypes() {
		let types: Array<any> = [];

		this.bannersList.forEach((banner: Banner) => {
			types.push(banner.type.name);
		});

		this.types = types.filter((item, index) => {
			return types.indexOf(item) === index;
		});

	}

	search() {
		let search = this.searchForm.controls['search'].value.trim().toLowerCase();
		let typeFilter = this.searchForm.controls['type'].value.trim().toLowerCase();
		let activeFilter = this.searchForm.controls['active'].value;

		this.bannersList = [...this.tmpBannersList];

		if (search !== '') {
			this.bannersList = this.bannersList.filter((banner: Banner) => {

				let title = banner.title;
				let summary = banner.summary;

				if (!title) title = '';
				if (!summary) summary = '';


				if (title.toLowerCase().includes(search) || summary.toLowerCase().includes(search)) return banner;
				else return;
			})
		}
		
		if (typeFilter !== '') {
			this.bannersList = this.bannersList.filter((banner: Banner) => {

				let type = banner.type.name;

				if (!type) type = '';


				if (type.toLowerCase().includes(typeFilter)) return banner;
				else return;
			})
		}
		
		if (activeFilter === "1" || activeFilter === "0") {
			this.bannersList = this.bannersList.filter((banner: Banner) => {

				let active = banner.active;
				activeFilter = parseInt(activeFilter);
				if (active === activeFilter) return banner;
				else return;
			})
		}

		/*if(search !== '' && typeFilter !== '' && (activeFilter === "1" || activeFilter === "0")){
			this.bannersList = [...this.tmpBannersList];
		}*/


	}


	deleteBanner(bannerId: number) {
		const dialogRef = this.dialog.open(DeleteModalComponent, {
			width: '300px',
			height: '180px',
			data: {
				title: 'Borrar Elementos',
				message: 'Una vez eliminados los elementos no podrán ser recuperados!'
			},
		});

		dialogRef.afterClosed().subscribe((deleteElement: boolean) => {
			if (deleteElement) {
				this.platformService.deleteBanner(bannerId).subscribe((response: any) => {
					this.bannersList = this.bannersList.filter((banner: Banner) => { return (banner.id !== bannerId)} ); // filtramos
				});
			}
		});
	}

}
