import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { Footer } from '../../models/footer';
import { ResponseFooter } from '../../models/response-footer';
import { PlatformService } from '../../services/platform.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public footerForm: FormGroup = new FormGroup({});
  private footerFormData: FormData = new FormData();

  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '400px',
    minHeight: '0',
    maxHeight: 'auto',
    width: '100%',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  constructor(
    private platformService: PlatformService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loadFooter();

    this.footerForm = this.formBuilder.group({
      htmlContent: ['']
    });
  }

  loadFooter(){
    this.platformService.getFooter().subscribe( (response: ResponseFooter) => {
      let footer = response.data[0];

      this.footerForm.patchValue({
        htmlContent: footer.text
      }); 

      this.footerFormData.append('id', footer.id.toString());    
      this.footerFormData.append('title', footer.title);
      this.footerFormData.append('text', footer.text);

    });
  }

  updateFooter() {
    this.footerFormData.set('text', this.footerForm.controls['htmlContent'].value);

    this.platformService.updateFooter(this.footerFormData).subscribe((response: Footer) => {
      this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });
    });

  }

}
