import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Config } from 'src/app/config/config';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { Banner } from '../../models/banner';
import { BannerTypes } from '../../models/banner-types';
import { ResponseBanner } from '../../models/response-banner';
import { ResponseBannerTypes } from '../../models/response-banner-types';
import { PlatformService } from '../../services/platform.service';

@Component({
  selector: 'app-detail-banner',
  templateUrl: './detail-banner.component.html',
  styleUrls: ['./detail-banner.component.scss']
})
export class DetailBannerComponent implements OnInit {
  public imgUrl = Config.env.imgUrl;

  public bannerSelected: Array<Banner> = [];
  public bannerTypes: Array<BannerTypes> = [];
  public bannerForm: FormGroup = new FormGroup({});
  public headerLogoSrc: string | SafeResourceUrl = '';
  private bannerFormData: FormData = new FormData();

  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '400px',
    minHeight: '0',
    maxHeight: 'auto',
    width: '100%',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };


  constructor(
    private activeRouter: ActivatedRoute,
    private platformService: PlatformService,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadBannerTypes();

    this.activeRouter.params.subscribe((params: any) => {
      this.loadBannerSelected(params.id);
    });


    this.bannerForm = this.formBuilder.group({
      type: ['', Validators.required],
      position: [0, Validators.min(0)],
      title: ['', Validators.required],
      summary: ['', Validators.required],
      active: ['', Validators.required],
      htmlContent: ['']
    });

  }

  loadBannerSelected(bannerId: string) {
    this.platformService.getBannerById(bannerId).subscribe((response: ResponseBanner) => {

      this.bannerSelected = response.data;
      console.log('Response: ', this.bannerSelected[0]);
      this.bannerForm.patchValue({
        type: this.bannerSelected[0].type.name,
        position: this.bannerSelected[0].position,
        title: this.bannerSelected[0].title,
        summary: this.bannerSelected[0].summary,
        active: this.bannerSelected[0].active,
        htmlContent: this.bannerSelected[0].text
      });

      this.bannerFormData.append('id', this.bannerSelected[0].id.toString());
      this.bannerFormData.append('position', this.bannerSelected[0].position.toString());
      this.bannerFormData.append('title', this.bannerSelected[0].title);
      this.bannerFormData.append('summary', this.bannerSelected[0].summary);
      this.bannerFormData.append('video', this.bannerSelected[0].video);
      this.bannerFormData.append('text', this.bannerSelected[0].text);
      this.bannerFormData.append('background', this.bannerSelected[0].background);
      this.bannerFormData.append('id_type_banner', this.bannerSelected[0].id_type_banner.toString());
      this.bannerFormData.append('active', this.bannerSelected[0].active.toString());
      this.bannerFormData.append('timestamp', this.bannerSelected[0].timestamp.toString());

      this.headerLogoSrc = this.imgUrl + this.bannerSelected[0].background;

    });
  }

  loadBannerTypes() {
    this.platformService.getBannerTypes().subscribe((response: ResponseBannerTypes) => {
      this.bannerTypes = response.data;
    });
  }

  getBackgroundImg(selectedFile: Array<any>) {
    selectedFile.forEach((backgroundImg: File) => {
      this.bannerFormData.set('background', backgroundImg);

      this.getBase64File(backgroundImg).then((base64: any): any => {
        const headerBlob = this.convertBase64ToBlob(base64.replace(`data:${backgroundImg.type};base64,`, ''), backgroundImg.type);
        this.headerLogoSrc = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(headerBlob));
      });

    });
  }

  filesRemoved(isFilesRemoved: boolean) {
    if (isFilesRemoved) {
      this.headerLogoSrc = '';
      this.bannerFormData.set('background', '');
      console.log('Remove Img');
      
    }
  }

  updateBanner() {
    this.bannerFormData.set('position', this.bannerForm.controls['position'].value);
    this.bannerFormData.set('title', this.bannerForm.controls['title'].value);
    this.bannerFormData.set('summary', this.bannerForm.controls['summary'].value);
    this.bannerFormData.set('text', this.bannerForm.controls['htmlContent'].value);
    this.bannerFormData.set('active', this.bannerForm.controls['active'].value);

    this.platformService.updateBanner(this.bannerFormData).subscribe( (response: Banner) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: { hola: 'mundo' },
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/general/banners'])
        
      });
      
    })

  }


  private getBase64File(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  private convertBase64ToBlob(b64Data: any, contentType = '', sliceSize = 512): Blob {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    console.log('Blob: ', blob);

    return blob;
  }

  /**Comparaciones para los selects*/
  compareObjects(o1: any, o2: any) {
    if (o2 !== null) {
      return o1 === o2;
    }
    return false;
  }


}
