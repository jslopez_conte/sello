

export interface Banner {
   id: number;
   title: string;
   summary: string;
   position: number;
   active: boolean;
   video: string;
   text: string;
   background: string;
   id_type_banner: number;
   timestamp: Date;
   type:{
      id: number;
      name: string;
   }
}