import { Banner } from "./banner";

export interface ResponseBanner {
   data: Array<Banner>;
   total_results: number;
}
