import { Footer } from "./footer";

export interface ResponseFooter {
   data: Array<Footer>;
   total_results: number;
}
