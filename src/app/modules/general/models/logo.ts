

export interface Logo {
   id: number;
   header: string | Blob;
   footer: string | Blob;
}