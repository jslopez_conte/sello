import { BannerTypes } from "./banner-types";

export interface ResponseBannerTypes {
   data: Array<BannerTypes>;
   total_results: number;
}
