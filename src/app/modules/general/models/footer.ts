

export interface Footer {
   id: number;
   title: string;
   text: string;
}