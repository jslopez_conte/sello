import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BannersComponent } from './pages/banners/banners.component';
import { CreateBannerComponent } from './pages/create-banner/create-banner.component';
import { DetailBannerComponent } from './pages/detail-banner/detail-banner.component';
import { FooterComponent } from './pages/footer/footer.component';
import { LogosComponent } from './pages/logos/logos.component';

const GENERAL_ROUTING: Routes = [{
   path: '',
   children: [
      {
         path: 'logos',
         component: LogosComponent,
      },
      {
         path: 'banners',
         component: BannersComponent,
      },
      {
         path: 'banners/details/:id',
         component: DetailBannerComponent,
      },
      {
         path: 'banners/create',
         component: CreateBannerComponent,
      },
      {
         path: 'footer',
         component: FooterComponent,
      },
      {
          path: '**',
          redirectTo: 'home'
      }
   ]
}];

@NgModule({
   imports: [RouterModule.forChild(GENERAL_ROUTING)],
   exports: [RouterModule]
})
export class GeneralRoutingModule { }
