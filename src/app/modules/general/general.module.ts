import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralRoutingModule } from './general-routing.module';
import { LogosComponent } from './pages/logos/logos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BannersComponent } from './pages/banners/banners.component';
import { DetailBannerComponent } from './pages/detail-banner/detail-banner.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CreateBannerComponent } from './pages/create-banner/create-banner.component';
import { FooterComponent } from './pages/footer/footer.component';


@NgModule({
  declarations: [
    LogosComponent,
    BannersComponent,
    DetailBannerComponent,
    CreateBannerComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularEditorModule,
    GeneralRoutingModule
  ]
})
export class GeneralModule { }
