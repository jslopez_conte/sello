import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Config } from 'src/app/config/config';
import { ResponseRequestStatus } from '../../manage/models/response-request-status';
import { ResponseEvaluator } from '../models/evaluator';
import { ResponseService, Service } from '../models/service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  
  private baseUrl: string = Config.env.apiUrl;
  
  constructor(
    private http: HttpClient
  ) { }

  private getQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.get(url, { headers });
  }

  private putQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.put(url, body,{ headers });
  }

  private postQuery( query: string, body: any ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.post(url, body,{ headers });
  }

  private deleteQuery( query: string ) {
    const url = this.baseUrl + query;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });

    return this.http.delete(url,{ headers });
  }

  getServices(page: string = "1"): Observable<ResponseService> {
    return this.getQuery('/service/service?simple=false&filter_field=current_status&filter_value=5&limit=10&page='+page)
      .pipe( map((data: any) => data));
  }

  getServiceById(serviceId: string): Observable<ResponseService> {
    return this.getQuery('/service/service?simple=false&limit=10&page=1&filter_field=id&filter_value='+serviceId)
      .pipe( map((data: any) => data));
  }


  getEvaluatorByRequirementId(requirementId: string): Observable<ResponseEvaluator> {
    return this.getQuery('/question/evaluation_request?simple=false&filter_field=id_answer&filter_value='+requirementId+'&limit=100&page=1')
      .pipe( map((data: any) => data));
  }

  updateService(body: FormData): Observable<Service> {
    return this.putQuery('/service/service', body)
      .pipe( map((data: any) => data));
  }

}
