import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAwardedComponent } from './no-awarded.component';

describe('NoAwardedComponent', () => {
  let component: NoAwardedComponent;
  let fixture: ComponentFixture<NoAwardedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoAwardedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NoAwardedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
