import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Banner } from 'src/app/modules/general/models/banner';
import { ResponseBanner } from 'src/app/modules/general/models/response-banner';
import { PlatformService } from 'src/app/modules/general/services/platform.service';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import { ResponseService, Service } from '../../models/service';
import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'app-no-awarded',
  templateUrl: './no-awarded.component.html',
  styleUrls: ['./no-awarded.component.scss']
})
export class NoAwardedComponent implements OnInit {
 
  	public servicesList: Array<Service> = [];
	public tmpServicesList: Array<Service> = [];
	public canShowMore: boolean = false;
	public numberPage: number = 1;
	public types: Array<any> = [];
	public searchForm: FormGroup = new FormGroup({});;

	constructor(
		private serviceService: ServiceService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog
	) { }
 
	ngOnInit(): void {
		this.searchForm = this.formBuilder.group({
			type: [''],
			active: [null],
			search: ['']
		});

		this.loadServices();
	}

	loadServices() {
		this.serviceService.getServices(this.numberPage.toString()).subscribe((response: ResponseService) => {

			if (response.total_results === 0) return;
			this.servicesList = [...this.servicesList, ...response.data];
			this.tmpServicesList = [...this.servicesList];
			this.getTypes();

			if (response.total_results > this.servicesList.length) {
				this.canShowMore = true;
				this.numberPage++;
			}
			else {
				this.canShowMore = false;
			}
		})
	}

	getTypes() {
		/*let types: Array<any> = [];

		this.servicesList.forEach((service: Service) => {
			types.push(banner.type.name);
		});

		this.types = types.filter((item, index) => {
			return types.indexOf(item) === index;
		});*/

	}

	search() {
	
	}


	deleteService(serviceId: number) {
		const dialogRef = this.dialog.open(DeleteModalComponent, {
			width: '300px',
			height: '180px',
			data: {
				title: 'Borrar Elementos',
				message: 'Una vez eliminados los elementos no podrán ser recuperados!'
			},
		});

		dialogRef.afterClosed().subscribe((deleteElement: boolean) => {
			/*
			if (deleteElement) {
				this.serviceService.getServices(bannerId).subscribe((response: any) => {
					this.servicesList = this.servicesList.filter((banner: Banner) => { return (banner.id !== bannerId)} ); // filtramos
				});
			}*/
		});
	}


}
