import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAwardedDetailsComponent } from './no-awarded-details.component';

describe('NoAwardedDetailsComponent', () => {
  let component: NoAwardedDetailsComponent;
  let fixture: ComponentFixture<NoAwardedDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoAwardedDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NoAwardedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
