import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { RequirementsResponseModalComponent } from '../../components/requirements-response-modal/requirements-response-modal.component';
import { RequirementsComponent } from '../../components/requirements/requirements.component';
import { ResponseService, Service } from '../../models/service';
import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'app-no-awarded-details',
  templateUrl: './no-awarded-details.component.html',
  styleUrls: ['./no-awarded-details.component.scss']
})
export class NoAwardedDetailsComponent implements OnInit {

  public service: Array<Service> = [];
  public showBasicData: boolean = true;
  public showHistory: boolean = false;
  public showCommits: boolean = false;
  public showRequirements: boolean = false;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceService: ServiceService
  ) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe((params: any) => {
      this.loadService(params.id);
    });

  }

  loadService(serviceId: string) {
    this.serviceService.getServiceById(serviceId).subscribe((response: ResponseService) => {

      if (!response.data.length) return;

      this.service.push(response.data[0]);
      console.log('Response: ', this.service[0]);

    });
  }

  changeView(key: number) {
    switch (key) {
      case 1:
        this.showBasicData = true;
        this.showHistory = false;
        this.showCommits = false;
        this.showRequirements = false;
        break;

      case 2:
        this.showBasicData = false;
        this.showHistory = true;
        this.showCommits = false;
        this.showRequirements = false;
        break;
      case 3:
        this.showBasicData = false;
        this.showHistory = false;
        this.showCommits = true;
        this.showRequirements = false;
        break;
      case 4:
        this.showBasicData = false;
        this.showHistory = false;
        this.showCommits = false;
        this.showRequirements = true;
        break;
      default:
        break;
    }
  }

}
