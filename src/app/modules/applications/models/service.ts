export interface ResponseService {
   data: Service[]
   total_results: number
 }
 
 export interface Service {
   id: number
   name: string
   id_category: number
   current_status: number
   level: number
   rate: number
   url: string
   id_institution: number
   test_user: any
   test_password: any
   is_active: number
   timestamp: string
   id_user: number
   category: Category
   institution: Institution
   status: Status
   history: History[]
   requisites: Requisite[]
 }
 
 export interface Category {
   id: number
   name: string
   diploma: number
   validity: number
   active: number
 }
 
 export interface Institution {
   id: number
   name: string
   nit: string
   website: any
   address?: string
   email: string
   phone: string
   extension_phone: any
   id_city: number
   id_region: number
   id_country: number
   id_institution_type: number
   active: number
   timestamp: string
 }
 
 export interface Status {
   id: number
   name: string
   duration: number
   pre_end: number
   alert: number
   description: string
 }
 
 export interface History {
   id: number
   id_service: number
   id_status: number
   level: number
   valid_to: string
   timestamp: string
   status: Status2
 }
 
 export interface Status2 {
   id: number
   name: string
   duration: number
   pre_end: number
   alert: number
   description: string
 }
 
 export interface Requisite {
   id: number
   id_order: any
   id_user: number
   id_service: number
   id_question: number
   id_topic: number
   id_status: number
   id_media?: number
   datetime: string
   timestamp: string
   comment: string
   alert: number
   service: Service
   question: Question
   user: User
   media: Media
   topic: Topic
   status: Status3
 }
 
 export interface Service {
   id: number
   name: string
   id_category: number
   current_status: number
   level: number
   rate: number
   url: string
   id_institution: number
   test_user: any
   test_password: any
   is_active: number
   timestamp: string
   id_user: number
 }
 
 export interface Question {
   id: number
   id_topic: number
   level: number
   text: string
   criteria: string
   evidence: string
   legal_support: string
   help: string
   active: number
 }
 
 export interface User {
   id: number
   name: string
   secondname: string
   lastname: string
   secondlastname: string
   email: string
   phone: string
   mobile: string
   organization: any
   ocupation: any
   tmp_pwd: number
   points: number
   active: number
   terms: number
   timestamp: string
   id_availability: any
   id_city: number
   id_region: number
   id_country: number
   document: string
   id_type_document: number
 }
 
 export interface Media {
   id?: number
   url?: string
   type?: string
   timestamp?: string
 }
 
 export interface Topic {
   id: number
   name: string
   description: string
   id_usertype: number
   id_category: number
   active: number
 }
 
 export interface Status3 {
   id: number
   name: string
   duration: number
   pre_end: number
   alert: number
   description: string
 }
 