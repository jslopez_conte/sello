export interface ResponseEvaluator {
   data: Evaluator[]
   total_results: number
 }
 
 export interface Evaluator {
   id: number
   id_user: number
   id_answer: number
   id_service: number
   id_request_status: number
   id_question: number
   timestamp: string
   result?: number
   branch: number
   justify_reject: any
   alert_time: string
   end_time: string
   user: User
   user_answer: UserAnswer
   question: Question
   service: Service
   status: Status
 }
 
 export interface User {
   id: number
   name: string
   secondname: string
   lastname: string
   secondlastname: string
   email: string
   phone: string
   mobile: string
   organization?: string
   ocupation?: string
   tmp_pwd: number
   points: number
   active: number
   terms: number
   timestamp: string
   id_availability?: number
   id_city?: number
   id_region?: number
   id_country?: number
   document?: string
   id_type_document?: number
 }
 
 export interface UserAnswer {
   id: number
   id_order: any
   id_user: number
   id_service: number
   id_question: number
   id_topic: number
   id_status: number
   id_media: number
   datetime: string
   timestamp: string
   comment: string
   alert: number
 }
 
 export interface Question {
   id: number
   id_topic: number
   level: number
   text: string
   criteria: string
   evidence: string
   legal_support: string
   help: string
   active: number
 }
 
 export interface Service {
   id: number
   name: string
   id_category: number
   current_status: number
   level: number
   rate: number
   url: string
   id_institution: number
   test_user: any
   test_password: any
   is_active: number
   timestamp: string
   id_user: number
 }
 
 export interface Status {
   id: number
   name: string
   duration: number
   pre_end: number
   alert: number
   description: string
 }
 