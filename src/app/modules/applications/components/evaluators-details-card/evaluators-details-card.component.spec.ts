import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatorsDetailsCardComponent } from './evaluators-details-card.component';

describe('EvaluatorsDetailsCardComponent', () => {
  let component: EvaluatorsDetailsCardComponent;
  let fixture: ComponentFixture<EvaluatorsDetailsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluatorsDetailsCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EvaluatorsDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
