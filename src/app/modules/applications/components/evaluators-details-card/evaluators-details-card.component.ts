import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/evaluator';

@Component({
  selector: 'app-evaluators-details-card',
  templateUrl: './evaluators-details-card.component.html',
  styleUrls: ['./evaluators-details-card.component.scss']
})
export class EvaluatorsDetailsCardComponent implements OnInit {

  @Input() evaluator: User;
 
  public cardForm: FormGroup = new FormGroup({});

  constructor(
    private formBuilder: FormBuilder
  ) {

    this.evaluator = {
      id: 0,
      name: '',
      secondname: '',
      lastname: '',
      secondlastname: '',
      email: '',
      phone: '',
      mobile: '',
      organization: '',
      ocupation: '',
      tmp_pwd: 0,
      points: 0,
      active: 0,
      terms: 0,
      timestamp: '',
      id_availability: 0,
      id_city: 0,
      id_region: 0,
      id_country: 0,
      document: '',
      id_type_document: 0,
    }
    
  }

  ngOnInit(): void {
    this.cardForm = this.formBuilder.group({
      name: [this.evaluator.name, Validators.required],
      secondName: [this.evaluator.secondname, Validators.required],
      lastName: [this.evaluator.lastname, Validators.required],
      secondLastName: [this.evaluator.secondlastname, Validators.required],
      email: [this.evaluator.email, Validators.required],
      phone: [this.evaluator.phone, Validators.required]
    });


    console.log('Test:' + this.evaluator);
    

  }



}
