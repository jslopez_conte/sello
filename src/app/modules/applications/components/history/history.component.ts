import { Component, Input, OnInit } from '@angular/core';
import { History } from '../../models/service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  @Input() histories: Array<History> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
