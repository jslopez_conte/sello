import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatorsDetailsComponent } from './evaluators-details.component';

describe('EvaluatorsDetailsComponent', () => {
  let component: EvaluatorsDetailsComponent;
  let fixture: ComponentFixture<EvaluatorsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluatorsDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EvaluatorsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
