import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from 'src/app/config/config';
import { Evaluator, ResponseEvaluator, User } from '../../models/evaluator';
import { Requisite, Question } from '../../models/service';
import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'app-evaluators-details',
  templateUrl: './evaluators-details.component.html',
  styleUrls: ['./evaluators-details.component.scss']
})
export class EvaluatorsDetailsComponent implements OnInit {

  @Input() requirement: Array<Requisite> = [];

  public cardForm: FormGroup = new FormGroup({});
  public evaluators: Array<User> = [];
  public baseUrl = Config.env.imgUrl;
  
  constructor(
    private serviceService: ServiceService
  ) { }

  ngOnInit(): void {  
    this.loadEvaluators();

  }


  loadEvaluators(){
    this.serviceService.getEvaluatorByRequirementId( this.requirement[0].id.toString() )
      .subscribe( (response: ResponseEvaluator) => {
        response.data.forEach((evaluator: Evaluator) => {
          this.evaluators.push(evaluator.user);
        });  
      })
  }

}
