import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementsResponseModalComponent } from './requirements-response-modal.component';

describe('RequirementsResponseModalComponent', () => {
  let component: RequirementsResponseModalComponent;
  let fixture: ComponentFixture<RequirementsResponseModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequirementsResponseModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RequirementsResponseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
