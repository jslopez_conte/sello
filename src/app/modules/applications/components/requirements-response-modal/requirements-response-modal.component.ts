import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-requirements-response-modal',
  templateUrl: './requirements-response-modal.component.html',
  styleUrls: ['./requirements-response-modal.component.scss']
})
export class RequirementsResponseModalComponent implements OnInit {

  public showDetails: boolean = true;
  public showRates: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<RequirementsResponseModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) { }
 
  ngOnInit(): void {
  
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  changeView(key: number) {
    switch (key) {
      case 1:
        this.showDetails = true;
        this.showRates = false;
        break;

      case 2:
        this.showDetails = false;
        this.showRates = true;
        break;
      default:
        break;
    }
  }

}
