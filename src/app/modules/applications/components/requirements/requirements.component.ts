import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Config } from 'src/app/config/config';
import { Question, Requisite, Service } from '../../models/service';
import { RequirementsResponseModalComponent } from '../requirements-response-modal/requirements-response-modal.component';

@Component({
  selector: 'app-requirements',
  templateUrl: './requirements.component.html',
  styleUrls: ['./requirements.component.scss']
})
export class RequirementsComponent implements OnInit {

  public baseUrl = Config.env.imgUrl;

  @Input() services: Array<Service> = [];

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  viewMore( requirement: Requisite ){

    const dialogRef = this.dialog.open(RequirementsResponseModalComponent, {
			width: '80%',
			height: '80%',
			data: {
				requirement: { ...requirement }
			},
		});

		dialogRef.afterClosed().subscribe((deleteElement: boolean) => {
		});
  }


}
