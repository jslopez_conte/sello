import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { InfoModalComponent } from 'src/app/shared/components/info-modal/info-modal.component';
import { Service } from '../../models/service';
import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'app-basic-data',
  templateUrl: './basic-data.component.html',
  styleUrls: ['./basic-data.component.scss']
})
export class BasicDataComponent implements OnInit {

  @Input() service: Array<Service> = [];

  public serviceForm: FormGroup = new FormGroup({});
  private serviceFormData: FormData = new FormData();

  constructor(
    private formBuilder: FormBuilder,
    private serviceService: ServiceService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.serviceForm = this.formBuilder.group({
      id: [this.service[0].id, Validators.min(0)],
      name: [this.service[0].name, Validators.required],
      category: [this.service[0].category.name, Validators.required],
      status: [this.service[0].status.name, Validators.required],
      level: [this.service[0].level, Validators.min(0)],
      rate: [this.service[0].rate, Validators.min(0)],
      url: [this.service[0].url, Validators.required],
      institution: [this.service[0].name, Validators.required],
      is_active: [this.service[0].is_active, Validators.required],
      timestamp: [this.service[0].timestamp, Validators.required]
    });

    this.serviceFormData.append('id', this.service[0].id.toString());
    this.serviceFormData.append('name', this.service[0].name);
    this.serviceFormData.append('category', this.service[0].category.id.toString());
    this.serviceFormData.append('level', this.service[0].level.toString());
    this.serviceFormData.append('url', this.service[0].url);
    this.serviceFormData.append('institution', this.service[0].id_institution.toString());
    this.serviceFormData.append('is_active', this.service[0].is_active.toString());
    this.serviceFormData.append('timestamp', this.service[0].timestamp);

  }



  updateService() {
    this.serviceFormData.set('name', this.serviceForm.controls['name'].value);
    this.serviceFormData.set('url', this.serviceForm.controls['url'].value);
    this.serviceFormData.set('is_active', this.serviceForm.controls['is_active'].value);


    this.serviceService.updateService(this.serviceFormData).subscribe( (response: Service) => {
      const dialogRef = this.dialog.open(InfoModalComponent, {
        width: '300px',
        height: '180px',
        data: {},
      });

      dialogRef.afterClosed().subscribe((userUpdate: any) => {  
        this.router.navigate(['/admin/applications/no-awarded/'])
        
      });
      
    })

  }

}
