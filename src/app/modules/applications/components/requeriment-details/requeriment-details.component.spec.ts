import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequerimentDetailsComponent } from './requeriment-details.component';

describe('RequerimentDetailsComponent', () => {
  let component: RequerimentDetailsComponent;
  let fixture: ComponentFixture<RequerimentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequerimentDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RequerimentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
