import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Config } from 'src/app/config/config';
import { Question, Requisite } from '../../models/service';

@Component({
  selector: 'app-requeriment-details',
  templateUrl: './requeriment-details.component.html',
  styleUrls: ['./requeriment-details.component.scss']
})
export class RequerimentDetailsComponent implements OnInit {

  @Input() requirement: Array<Requisite> = [];
  public questionForm: FormGroup = new FormGroup({});

  public baseUrl = Config.env.imgUrl;
  
  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    let question: Question = this.requirement[0].question;
    
    this.questionForm = this.formBuilder.group({
      id: [question.id, Validators.min(0)],
      requirement: [question.text, Validators.required],
      criteria: [question.criteria, Validators.required],
      evidence: [question.evidence, Validators.required],
      entityComment: [this.requirement[0].comment, Validators.min(0)]
    });

  }


}
