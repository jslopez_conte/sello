import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoAwardedDetailsComponent } from './pages/no-awarded-details/no-awarded-details.component';
import { NoAwardedComponent } from './pages/no-awarded/no-awarded.component';

const APPLICATIONS_ROUTING: Routes = [{
  path: '',
  children: [
    {
      path: 'no-awarded',
      component: NoAwardedComponent,
    },
    {
      path: 'no-awarded/details/:id',
      component: NoAwardedDetailsComponent,
    },
    {
      path: '**',
      redirectTo: 'home'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(APPLICATIONS_ROUTING)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
