import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { NoAwardedComponent } from './pages/no-awarded/no-awarded.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NoAwardedDetailsComponent } from './pages/no-awarded-details/no-awarded-details.component';
import { BasicDataComponent } from './components/basic-data/basic-data.component';
import { HistoryComponent } from './components/history/history.component';
import { CommitsComponent } from './components/commits/commits.component';
import { RequirementsComponent } from './components/requirements/requirements.component';
import { RequirementsResponseModalComponent } from './components/requirements-response-modal/requirements-response-modal.component';
import { RequerimentDetailsComponent } from './components/requeriment-details/requeriment-details.component';
import { EvaluatorsDetailsComponent } from './components/evaluators-details/evaluators-details.component';
import { EvaluatorsDetailsCardComponent } from './components/evaluators-details-card/evaluators-details-card.component';


@NgModule({
  declarations: [
    NoAwardedComponent,
    NoAwardedDetailsComponent,
    BasicDataComponent,
    HistoryComponent,
    CommitsComponent,
    RequirementsComponent,
    RequirementsResponseModalComponent,
    RequerimentDetailsComponent,
    EvaluatorsDetailsComponent,
    EvaluatorsDetailsCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationsRoutingModule
  ]
})
export class ApplicationsModule { }
